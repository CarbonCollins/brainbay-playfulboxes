![MindLAB logo](https://i.ibb.co/ZJJY7S7/Original-200x200.png "MindLAB logo") 
# MindLAB's BrainBay-PlayfulBoxes
This repository hosts [brainbay](https://www.google.com "Brainbay's Homepage")'s projects developed at [MindLAB.space](http://mindlab.space "MindLAB's Homepage").   

The repository is organized in "designs". Every design is an independent project/application and has its own folder.   
Every project's folders contain the BrainBay configuration (&lt;configuration-name>.con files), related documentation (&lt;configuration-name>.md files) and maybe some additional files.


## Getting Started
### Prerequisites
In general, for all the configurations: 
- Windows (>= XP), 
- BrainBay (>= 2.3.1, as the configuration has been designed to work with with OpenBCI hub / ganglion devices with BLED112 bluetooth dongle)   

Configuration's specific prerequisites are specified in the specific documentation file.


### Installing
In general, load a specific design file (the configuration file) from inside BrainBay.  
Specific instructions can then be found in the related documentation file. 


## Contributing
When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

### Pull Request Process
1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
2. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent. The versioning scheme we use is SemVer.
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.


### Code of Conduct
#### Our Pledge
In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.


#### Our Standards
Examples of behavior that contributes to creating a positive environment include:  

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:   

- The use of sexualized language or imagery and unwelcome sexual attention or advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting


## Versioning
We use SemVer for versioning. For the versions available, see the tags on this repository.


## Authors
- The [MindLAB.space](http://mindlab.space "MindLAB's Homepage") team.   
Read about the contributors of specific designs into the related documentation files.


## License
All the design are licensed under the 
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.   


