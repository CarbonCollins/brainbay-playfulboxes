![MindLAB logo](https://i.ibb.co/ZJJY7S7/Original-200x200.png "MindLAB logo") 
# MindSync Alpha Beta Theta MIDI Player

With this [brainbay](https://www.google.com "Brainbay's Homepage")'s configuration, your mind will play virtual midi instruments using your brainwaves!  

With the help of EEG sensors and computer algorithms, your brainwaves, will become virtual
hands that will play music notes.   
Your mind will learn to play different sounds and follow the music rhythm, without you knowing exactly how you do: as the most natural thing.   

## Astonishing MindSync

If another source of music is heard, like for example a downtempo song, people might engage mind-synchronization with it: usually it is quite natural that the subject's mind will change the intensity of the three different mental states at rhythm with the played music.   
If a constant beat is played, the mind will start playing "in sync" with it.   
The astonishing part? The subject is not completely conscious about how it is happening.   

The result varies on subject's disposition of mind: if full rested, feeling comfortable and enjoying the moment, then the synchronization with the played rhythm will be evident. 


## Data flow
![Data flow graph](https://i.ibb.co/SNBrML8/Mind-Sync-ABT-MIDI-Player-neurofeedback.png "Data flow graph") 

## Getting Started
### Prerequisites
- Windows (>= XP), 
- BrainBay (>= 2.3.1, as the configuration has been designed with OpenBCI hub / ganglion devices with BLED112 bluetooth dongle)  

### Installing
Load the specific design file (MindSync_-_ABT_-_MIDI_-_Player.md) from inside BrainBay.  

### Using
- Once the design has been loaded, set-up your connection to the OpenBCI Ganglion board. In the design window, right-click on the Ganglion element box. The element property window will appear. In the element property window press the button "scan for ganglion" to let BrainBay to find the ganglion board device. Select the device from the drop-down menu and then press the button "Connect". You can then close the Ganglion element property windows.
- Wear the EEG headset, with channel 1 (grey wire) and 2 (purple wiere) connected to ganglion board. The sensor should be placed in FP1 and FP2 (ten-twenty convention-map).
Connect the yellow connector and the black connector to the REF and D_G plugs on the Ganglion Board, and to your left and right hear respectively.
- Now you are set. It is time to play.
- Press F7 (Play). The EEG signal will be processed and transformed in Midi notes, that will be played by windows virtual midi instruments. The more there is a variation in voltage power for a specific brain-wave (alpha, beta or theta), the higer a note will be played by the instrument associated to that brain-wave.
Have fun!

## Contributing
When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

### Pull Request Process
1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
2. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent. The versioning scheme we use is SemVer.
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.


### Code of Conduct
#### Our Pledge
In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.


#### Our Standards
Examples of behavior that contributes to creating a positive environment include:  

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:   

- The use of sexualized language or imagery and unwelcome sexual attention or advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting


## Versioning
We use SemVer for versioning. For the versions available, see the tags on this repository.


## Authors
- The [MindLAB.space](http://mindlab.space "MindLAB's Homepage") team.   
Read about the contributors of specific designs into the related documentation files.


## License
All the design are licensed under the 
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.  

